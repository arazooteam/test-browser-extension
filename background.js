var targetPage = "https://test-arazoo.com/*";

var browser = browser || chrome

function addArazooHeader(e) {
  e.requestHeaders.push({"name": 'Test-Arazoo-Access', 
                           "value": 'mewBn3RfwVxgEY23Yfpnh'});
  return {requestHeaders: e.requestHeaders};
}

browser.webRequest.onBeforeSendHeaders.addListener(
  addArazooHeader,
  {"urls":[targetPage]},
  ["blocking", "requestHeaders"]
);